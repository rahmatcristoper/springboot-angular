package com.example.demo.repository;

import com.example.demo.model.UsersDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface UserJpaRepository extends JpaRepository<UsersDTO,Long> {

    UsersDTO findByName (String name);
    Optional<UsersDTO> findById(Long id);

}