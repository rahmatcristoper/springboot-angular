package com.example.demo.controller;

import com.example.demo.ErrorException.CustomErrorType;
import com.example.demo.model.UsersDTO;
import com.example.demo.repository.UserJpaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/user")
public class UserRegistrationRestController {

    public static final Logger logger = LoggerFactory.getLogger(UserRegistrationRestController.class);
    private UserJpaRepository userJpaRepository;

    @Autowired
    public void setUserJpaRepository(UserJpaRepository userJpaRepository) {
        this.userJpaRepository = userJpaRepository;
    }

    @GetMapping("/")
    public ResponseEntity<List<UsersDTO>> listAllUsers() {
        List<UsersDTO> users= userJpaRepository.findAll();
        if(users.isEmpty()) {
            return new ResponseEntity<List<UsersDTO>>(HttpStatus.NO_CONTENT);
        }
        return  new ResponseEntity<List<UsersDTO>>(users, HttpStatus.OK);
    }

    @PostMapping(value ="/", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UsersDTO> createUser(@Valid @RequestBody final UsersDTO user) {
        logger.info("Creating User : {}", user);
        if(userJpaRepository.findByName(user.getName()) != null) {
            logger.error("Unable to Create. A User with name {} already exist", user.getName());
            return new ResponseEntity<>(new CustomErrorType(
                    "Unable to create new User. " + user.getName() +
                            " already exist. "), HttpStatus.CONFLICT);
        }
        userJpaRepository.save(user);
        return new ResponseEntity<UsersDTO>(user, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<UsersDTO>> getUserById(@PathVariable("id") final Long id) {
        Optional<UsersDTO> user = userJpaRepository.findById(id);
        if(user.isEmpty()) {
            return new ResponseEntity<Optional<UsersDTO>>(Optional.of(new CustomErrorType(
                    "User with id " + id + " not found")), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Optional<UsersDTO>>(user, HttpStatus.OK);
    }

    @PutMapping(value="/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Optional<UsersDTO>> updateUser (@PathVariable("id") final Long id, @RequestBody UsersDTO user) {
        Optional<UsersDTO> currentUser = userJpaRepository.findById(id);

        if(currentUser.isEmpty()){
        return  new ResponseEntity<Optional<UsersDTO>>(Optional.of( new CustomErrorType(
                "Unable to update. User with id " + id + " Not found")),
                HttpStatus.NOT_FOUND);
        }
        UsersDTO updateUser = currentUser.get();
        updateUser.setName(user.getName());
        updateUser.setAddress(user.getAddress());
        updateUser.setEmail(user.getEmail());
        userJpaRepository.save(updateUser);
        return new ResponseEntity<Optional<UsersDTO>>(currentUser,HttpStatus.OK);

    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<UsersDTO> deleteUser (@PathVariable("id") final Long id) {
        Optional<UsersDTO> currentUser = userJpaRepository.findById(id);
        if(currentUser.isPresent()) {
            userJpaRepository.deleteById(id);
        }
        else {
            return new ResponseEntity<UsersDTO>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<UsersDTO>(HttpStatus.NO_CONTENT);
    }
}
