package com.example.demo.ErrorException;

import com.example.demo.model.UsersDTO;

public class CustomErrorType extends UsersDTO {

    private String errorMessage;

    public  CustomErrorType(final String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage () {
        return errorMessage;
    }

}
